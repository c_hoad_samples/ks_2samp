#include <iostream>
#include <random>
#include <vector>

#ifdef USE_ROOT
#include <root/TMath.h>
#endif

#include "ks.hpp"

int main()
{
    std::random_device rd;
    std::mt19937 mt{rd()};
    std::normal_distribution<double> d{1, 1};
    const auto gen{[&d, &mt]{return d(mt);}};
    std::vector<double> a(1000);
    std::vector<double> aw(1000);
    std::vector<double> b(1000);
    std::vector<double> bw(1000);

    std::generate(a.begin(), a.end(), gen);
    std::generate(b.begin(), b.end(), gen);
    std::generate(aw.begin(), aw.end(), gen);
    std::generate(bw.begin(), bw.end(), gen);

    auto ks{ks_2samp(a, b, aw, bw)};

    std::cout << ks.first << '\t' << ks.second << std::endl;

    return 0;
}
