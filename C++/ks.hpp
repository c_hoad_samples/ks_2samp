#include <algorithm>
#include <array>
#include <cstdlib>
#include <iterator>
#include <utility>
#include <vector>

#ifdef USE_ROOT
#include "TMath.h"
#endif

template <class T, class U>
auto ecdf(std::vector<T> x, std::vector<U> xw)
{
    // First create a vector wich pairs every measurement with its weight
    std::vector<std::pair<T, U>> m;
    m.reserve(x.size());
    std::transform(x.begin(), x.end(), xw.begin(), std::back_inserter(m),
                   [](const auto& a, const auto& b)
                   {return std::make_pair(a, b);});

    // Sort by measurement
    std::sort(m.begin(), m.end(), [] (const auto& a, const auto& b)
              {return a.first < b.first;});

    // Accumulate and normalise weights
    for(size_t i{1}; i < m.size(); ++i)
    {
        m.at(i).second += m.at(i - 1).second;
    }
    std::transform(m.begin(), m.end(), m.begin(),
                   [&m](const auto& p)
                   {return std::make_pair(p.first,
                                          p.second / m.back().second);});

    // Create step function
    return [measurements = std::move(m)] (const T val) -> U
        {
            auto it {std::upper_bound(measurements.begin(),
                                      measurements.end(), val,
                                      [](const T a,
                                          const std::pair<T, U> b)
                                      {return a < b.first;})};
            --it;
            if (it >= measurements.begin())
            {
                return it->second;
            }
            else
            {
                return 0;
            }
        };
}

#ifndef USE_ROOT
double kolmogorov_prob(const double z)
{
    // Adapted from the implementation found in CERN's ROOT software

    const std::array<double, 4> fj{-2, -8, -18, -32};
    constexpr double w{2.50662827};
    constexpr double c1{-1.2337005501361697};  // pi * 2/8
    constexpr double c2{9 * c1};
    constexpr double c3{25 * c1};
    const double u{std::abs(z)};

    if (u < 0.2)
    {
        return 1;
    }
    else if (u < 0.7555)
    {
        const double v{1. / std::pow(u , 2)};
        return 1 - w * (std::exp(c1 * v) + std::exp(c2 * v) + std::exp(c3 * v))
                        / u;
    }
    else if (u < 6.8116)
    {
        std::array<double, 4> r{};
        const double v{std::pow(u, 2)};
        const int maxj{std::max(1, static_cast<int>(3. / u + 0.5))};
        for (size_t j{0}; j < maxj; j++)
        {
            r[j] = std::exp(fj[j] * v);
        }
        return 2 * (r[0] - r[1] + r[2] - r[3]);
    }
    else
    {
        return 0;
    }
}
#endif

template <class T, class U>
std::pair<U, double> ks_2samp(const std::vector<T>& a,
                              const std::vector<T>& b,
                              const std::vector<U>& aw,
                              const std::vector<U>& bw,
                              const bool stephens = false)
{
    // Methodology for weighted Kolmogorov-Smirnov test taken from Numerical
    // Methods of Statistics - J. Monahan

    std::vector<T> ab{a};
    std::vector<U> a_ecdf;
    std::vector<U> b_ecdf;
    std::vector<U> ecdf_diff;

    ab.insert(ab.end(), b.begin(), b.end());
    std::sort(ab.begin(), ab.end());

    a_ecdf.reserve(ab.size());
    b_ecdf.reserve(ab.size());
    ecdf_diff.reserve(ab.size());
    std::transform(ab.begin(), ab.end(), std::back_inserter(a_ecdf),
                   ecdf(a, aw));
    std::transform(ab.begin(), ab.end(), std::back_inserter(b_ecdf),
                   ecdf(b, bw));
    std::transform(a_ecdf.begin(), a_ecdf.end(), b_ecdf.begin(),
                   std::back_inserter(ecdf_diff),
                   [](const auto& x, const auto& y)
                   {return std::abs(x - y);});

    const U D{*std::max_element(ecdf_diff.begin(), ecdf_diff.end())};
    const double n1{std::pow(std::accumulate(aw.begin(), aw.end(), 0.), 2) /
            std::accumulate(aw.begin(), aw.end(), 0.,
                            [](const auto& x, const auto& y)
                            {return x + std::pow(y, 2);})};
    const double n2{std::pow(std::accumulate(bw.begin(), bw.end(), 0.), 2) /
            std::accumulate(bw.begin(), bw.end(), 0.,
                            [](const auto& x, const auto& y)
                            {return x + std::pow(y, 2);})};
    const double en{std::sqrt(n1 * n2 / (n1 + n2))};

    // Stephens (1970)
    const double t{stephens ? (en + 0.12 + 0.11 / en) : en};

#ifdef USE_ROOT
    const double p{TMath::KolmogorovProb(t * D)};
#else
    const double p{kolmogorov_prob(t * D)};
#endif

    return {D, p};
}
